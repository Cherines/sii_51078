// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "Plano.h"
#include "Vector2D.h"
#include "Esfera.h"

class Raqueta : public Plano  
{
	
public:

	Vector2D velocidad;
	Vector2D getpos();
	Vector2D getvel();

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
	
	
};
