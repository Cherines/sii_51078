// Mundo.cpp: implementation of the CMundo class.
//Mercedes Alonso 51078
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include <sys/mman.h>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <errno.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	
}

CMundo::~CMundo()
{
	char buffer[60];
	int nbytes;

	sprintf(buffer, "out");
	nbytes = write(fd, buffer, sizeof(char)*60);
	if(nbytes <0 ) 
		{printf("error escribiendo :-1");
		 exit(1);
		}
	if(nbytes != sizeof(char)*60) 
		{printf("error escribiendo n distinto");
		 exit(1);
		}
	
	int error_f = 0;
	error_f = close (fd);
	if(error_f) printf("error al cerrar la fifo");

	error_f = unlink("/tmp/tub");
	if(error_f) printf("error al unlink la fifo");

	rmdir("/tmp/tub");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	int nbytes;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	char buffer[60];
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		//Enviar los datos por la tuberia
		sprintf(buffer, "Jugador 2 marca 1 punto, lleva un total de %d puntos", puntos2);
		nbytes = write(fd, buffer, sizeof(char)*60);
		if(nbytes <0 ) printf("error escribiendo :-1");
		if(nbytes != sizeof(char)*60) printf("error escribiendo n distinto");
		

	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		sprintf(buffer, "Jugador 1 marca 1 punto, lleva un total de %d puntos", puntos1);
		write(fd, buffer, sizeof(char)*60);
		
	}


	//parte del bot
	pdatos->esfera = esfera;
	pdatos->raqueta1 = jugador1;

	switch(pdatos->accion)
	{

		case 1 : OnKeyboardDown('s', 0, 0) ; break;

		case 0 : break;

		case -1 : OnKeyboardDown('w', 0, 0); break;

	}

	if((puntos1 || puntos2) == 3) exit(1);


	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'q':
		{
		 Vector2D vel;
		 vel.x = 4;
		 vel.y = 0;
		 Esfera *d = new Esfera(vel, jugador1.getpos());  
		// disp.agregar(d); 
		 break;
		}
	}





}

void CMundo::Init()
{
	//tuberia
	fd = open("/tmp/tub", O_RDWR);
	if(fd < 0)
		{ printf("Error al abrir la tuberia"); exit(1);}
		

	
	int fd_f = 0;
	//fichero
//	fd_f = open("/tmp/fbot", O_RDWR|O_CREAT|O_TRUNC, 666);
	fd_f = open("/tmp/fbot", O_RDWR|O_CREAT|O_TRUNC,S_IRWXU|S_IRWXG|S_IRWXO);
	
	if(fd_f < 0) 
		{perror("Error al abrir el fichero"); exit(1);}

	int aux_lenght=ftruncate(fd_f,sizeof(DatosMemCompartida));
	if(aux_lenght < 0) 
		{perror("Error al truncar el fichero"); exit(1);}

	pdatos = (DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE, MAP_SHARED, fd_f, 0);

	if(pdatos == ((void *) -1)) 
		{printf("Error al proyectar"); exit(1);}

	//&datos = pdatos;
	close(fd_f);
	


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
