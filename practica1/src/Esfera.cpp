// Esfera.cpp: implementation of the Esfera class.
// Mercedes Alonso 51078
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::Esfera(Vector2D vel, Vector2D cen)
{
	radio = 0.25f;
	centro = cen;
	velocidad = vel;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro = centro + velocidad * t;
	
	if(radio<0.01) radio = 0.5;
	else radio = radio - t*0.01;

	
}
