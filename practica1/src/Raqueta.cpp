// Raqueta.cpp: implementation of the Raqueta class.
//Mercedes Alonso 51078
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	x1 = x1 + velocidad.x * t;
	x2 = x2 + velocidad.x * t;
	y1 = y1 + velocidad.y * t;
	y2 = y2 + velocidad.y * t;
}

Vector2D Raqueta::getvel()
{
	return velocidad;
}

Vector2D Raqueta::getpos()
{
	Vector2D pos;
	pos.x = (x1 + x2 ) / 2;
	pos.y = (y1 + y2 ) / 2;
	return pos;
}
